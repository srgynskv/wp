<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package avtozap
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	/**
	 * @hooked avtozap_page_header - 10
	 * @hooked avtozap_page_content - 20
	 */
	do_action( 'avtozap_page' );
	?>
</article><!-- #post-## -->
