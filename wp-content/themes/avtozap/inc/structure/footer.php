<?php
/**
 * Template functions used for the site footer.
 *
 * @package avtozap
 */

if ( ! function_exists( 'avtozap_footer_widgets' ) ) {
	/**
	 * Display the footer widget regions
	 * @since  1.0.0
	 * @return  void
	 */
	function avtozap_footer_widgets() {
		if ( is_active_sidebar( 'footer-4' ) ) {
			$widget_columns = apply_filters( 'avtozap_footer_widget_regions', 4 );
		} elseif ( is_active_sidebar( 'footer-3' ) ) {
			$widget_columns = apply_filters( 'avtozap_footer_widget_regions', 3 );
		} elseif ( is_active_sidebar( 'footer-2' ) ) {
			$widget_columns = apply_filters( 'avtozap_footer_widget_regions', 2 );
		} elseif ( is_active_sidebar( 'footer-1' ) ) {
			$widget_columns = apply_filters( 'avtozap_footer_widget_regions', 1 );
		} else {
			$widget_columns = apply_filters( 'avtozap_footer_widget_regions', 0 );
		}

		if ( $widget_columns > 0 ) : ?>

			<section class="footer-widgets col-<?php echo intval( $widget_columns ); ?> fix">

				<?php $i = 0; while ( $i < $widget_columns ) : $i++; ?>

					<?php if ( is_active_sidebar( 'footer-' . $i ) ) : ?>

						<section class="block footer-widget-<?php echo intval( $i ); ?>">
				        	<?php dynamic_sidebar( 'footer-' . intval( $i ) ); ?>
						</section>

			        <?php endif; ?>

				<?php endwhile; ?>

			</section><!-- /.footer-widgets  -->

		<?php endif;
	}
}

if ( ! function_exists( 'avtozap_credit' ) ) {
	/**
	 * Display the theme credit
	 * @since  1.0.0
	 * @return void
	 */
	function avtozap_credit() {
		?>
		<div class="site-info">
			<?php echo esc_html( apply_filters( 'avtozap_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' 2012&ndash;' . date( 'Y' ) ) ); ?>
			<br>
			<?php printf( __( 'Created and maintained by', 'avtozap' ) ); ?>
			<a href="<?php echo esc_url( 'http://noskov.biz/' ); ?>" rel="designer" target="_blank"><?php _e( 'Serge Noskov', 'avtozap' ); ?></a>
		</div><!-- .site-info -->
		<?php
	}
}
