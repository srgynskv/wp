<?php
/**
 * Template functions used for the site header.
 *
 * @package avtozap
 */

if ( ! function_exists( 'avtozap_header_widget_region' ) ) {
	/**
	 * Display header widget region
	 * @since  1.0.0
	 */
	function avtozap_header_widget_region() {
		?>
		<div class="header-widget-region">
			<div class="col-full">
				<?php dynamic_sidebar( 'header-1' ); ?>
			</div>
		</div>
		<?php
	}
}

if ( ! function_exists( 'avtozap_site_branding' ) ) {
	/**
	 * Display Site Branding
	 * @since  1.0.0
	 * @return void
	 */
	function avtozap_site_branding() {
		if ( function_exists( 'jetpack_has_site_logo' ) && jetpack_has_site_logo() ) {
			jetpack_the_site_logo();
		} else { ?>
			<div class="site-branding">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<p class="site-description"><?php bloginfo( 'description' ); ?></p>
			</div>
		<?php }
	}
}

if ( ! function_exists( 'avtozap_primary_navigation' ) ) {
	/**
	 * Display Primary Navigation
	 * @since  1.0.0
	 * @return void
	 */
	function avtozap_primary_navigation() {
		?>
		<nav id="site-navigation" class="main-navigation" role="navigation">
		<button class="menu-toggle"><?php apply_filters( 'avtozap_menu_toggle_text', $content = _e( 'Primary Menu', 'avtozap' ) ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</nav><!-- #site-navigation -->
		<?php
	}
}

if ( ! function_exists( 'avtozap_secondary_navigation' ) ) {
	/**
	 * Display Secondary Navigation
	 * @since  1.0.0
	 * @return void
	 */
	function avtozap_secondary_navigation() {
		?>
		<nav class="secondary-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'fallback_cb' => '' ) ); ?>
		</nav><!-- #site-navigation -->
		<?php
	}
}
