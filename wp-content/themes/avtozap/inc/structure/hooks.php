<?php
/**
 * avtozap hooks
 *
 * @package avtozap
 */

/**
 * General
 * @see  avtozap_setup()
 * @see  avtozap_widgets_init()
 * @see  avtozap_scripts()
 * @see  avtozap_header_widget_region()
 * @see  avtozap_get_sidebar()
 */
add_action( 'after_setup_theme',			'avtozap_setup' );
add_action( 'widgets_init',					'avtozap_widgets_init' );
add_action( 'wp_enqueue_scripts',			'avtozap_scripts',				10 );
add_action( 'avtozap_before_content',	'avtozap_header_widget_region',	10 );
add_action( 'avtozap_sidebar',			'avtozap_get_sidebar',			10 );

/**
 * Header
 * @see  avtozap_secondary_navigation()
 * @see  avtozap_site_branding()
 * @see  avtozap_primary_navigation()
 */
add_action( 'avtozap_header', 'avtozap_site_branding',			20 );
add_action( 'avtozap_header', 'avtozap_secondary_navigation',		30 );
add_action( 'avtozap_header', 'avtozap_primary_navigation',		50 );

/**
 * Footer
 * @see  avtozap_footer_widgets()
 * @see  avtozap_credit()
 */
add_action( 'avtozap_footer', 'avtozap_footer_widgets',	10 );
add_action( 'avtozap_footer', 'avtozap_credit',			20 );

/**
 * Homepage
 * @see  avtozap_homepage_content()
 * @see  avtozap_product_categories()
 * @see  avtozap_recent_products()
 * @see  avtozap_featured_products()
 * @see  avtozap_popular_products()
 * @see  avtozap_on_sale_products()
 */
add_action( 'homepage', 'avtozap_homepage_content',		10 );
add_action( 'homepage', 'avtozap_product_categories',	20 );
add_action( 'homepage', 'avtozap_recent_products',		30 );
add_action( 'homepage', 'avtozap_featured_products',		40 );
add_action( 'homepage', 'avtozap_popular_products',		50 );
add_action( 'homepage', 'avtozap_on_sale_products',		60 );

/**
 * Posts
 * @see  avtozap_post_header()
 * @see  avtozap_post_meta()
 * @see  avtozap_post_content()
 * @see  avtozap_paging_nav()
 * @see  avtozap_single_post_header()
 * @see  avtozap_post_nav()
 * @see  avtozap_display_comments()
 */
add_action( 'avtozap_loop_post',			'avtozap_post_header',		10 );
add_action( 'avtozap_loop_post',			'avtozap_post_meta',			20 );
add_action( 'avtozap_loop_post',			'avtozap_post_content',		30 );
add_action( 'avtozap_loop_after',		'avtozap_paging_nav',		10 );
add_action( 'avtozap_single_post',		'avtozap_post_header',		10 );
add_action( 'avtozap_single_post',		'avtozap_post_meta',			20 );
add_action( 'avtozap_single_post',		'avtozap_post_content',		30 );
add_action( 'avtozap_single_post_after',	'avtozap_post_nav',			10 );
add_action( 'avtozap_single_post_after',	'avtozap_display_comments',	10 );

/**
 * Pages
 * @see  avtozap_page_header()
 * @see  avtozap_page_content()
 * @see  avtozap_display_comments()
 */
add_action( 'avtozap_page', 			'avtozap_page_header',		10 );
add_action( 'avtozap_page', 			'avtozap_page_content',		20 );
add_action( 'avtozap_page_after', 	'avtozap_display_comments',	10 );

/**
 * Extras
 * @see  avtozap_setup_author()
 * @see  avtozap_wp_title()
 * @see  avtozap_body_classes()
 * @see  avtozap_page_menu_args()
 */
add_action( 'wp',					'avtozap_setup_author' );
add_filter( 'body_class',			'avtozap_body_classes' );
add_filter( 'wp_page_menu_args',	'avtozap_page_menu_args' );
