<?php
/**
 * avtozap customizer hooks
 *
 * @package avtozap
 */

add_action( 'customize_preview_init', 	'avtozap_customize_preview_js' );
add_action( 'customize_register', 		'avtozap_customize_register' );
add_filter( 'body_class', 				'avtozap_layout_class' );
add_action( 'wp_enqueue_scripts', 		'avtozap_add_customizer_css', 130 );
add_action( 'after_setup_theme', 		'avtozap_custom_header_setup' );
