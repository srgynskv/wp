<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );
/**
 * Plugin Name: o2
 * Description: Must use plugin.
 * Plugin URI:
 * Author: Serge Noskov
 * Author URI: http://noskov.biz/
 * Version: 1.0
 * Text Domain: o2
 * Domain Path: /languages/
 */
define( 'O2INC', plugin_dir_path( __FILE__ ) . 'inc/' );
define( 'O2CSS', plugin_dir_url( __FILE__ ) . 'css/' );
define( 'O2JS', plugin_dir_url( __FILE__ ) . 'js/' );

include_once O2INC . 'functions.php';
include_once O2INC . 'translit.php';
include_once O2INC . 'shortcodes.php';

include_once O2INC . 'site.php';
include_once O2INC . 'service.php';
include_once O2INC . 'faq.php';
include_once O2INC . 'project.php';
include_once O2INC . 'testimonial.php';
include_once O2INC . 'event.php';
include_once O2INC . 'product.php';
include_once O2INC . 'product-used.php';
