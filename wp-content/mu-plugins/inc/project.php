<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Registers Project CPT.
 */
function o2_register_cpt_project() {
	$labels = array(
		'name'					=> __( 'Projects', 'o2' ),
		'singular_name'			=> __( 'Project', 'o2' ),
		'menu_name'				=> __( 'Projects', 'o2' ),
		'add_new'				=> __( 'Add New Project', 'o2' ),
		'add_new_item'			=> __( 'Add New Project', 'o2' ),
		'edit_item'				=> __( 'Edit Project', 'o2' ),
		'new_item'				=> __( 'New Project', 'o2' ),
		'view_item'				=> __( 'View Project', 'o2' ),
		'search_items'			=> __( 'Search Projects', 'o2' ),
		'not_found'				=> __( 'No Projects found', 'o2' ),
		'not_found_in_trash'	=> __( 'No Projects found in Trash', 'o2' ),
		'all_items'				=> __( 'All Projects', 'o2' ),
		'parent_item'			=> __( 'Parent Project', 'o2' ),
		'parent_item_colon'		=> __( 'Parent Project:', 'o2' ),
	);
	$args = array(
		'labels'				=> $labels,
		'label'					=> __( 'projects', 'o2' ),
		'description'			=> __( 'Project CPT', 'o2' ),
		'hierarchical'			=> false,
		'taxonomies'			=> array(),
		'public'				=> true,
		'publicly_queryable'	=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> null,
		'menu_icon'				=> null,
		'show_in_nav_menus'		=> false,
		'exclude_from_search'	=> false,
		'has_archive'			=> true,
		'query_var'				=> true,
		'can_export'			=> true,
		'delete_with_user'		=> false,
		'capability_type'		=> 'post',
		'supports'				=> array(
			'title', 'editor', 'excerpt', 'thumbnail',
			// 'title', 'editor', 'excerpt', 'thumbnail', 'post-formats', 'author', 'comments', 'trackbacks', 'custom-fields', 'page-attributes', 'revisions',
		),
		'rewrite' => array(
			'slug'			=> 'projects',
			'with_front'	=> false,
			'pages'			=> true,
			'feeds'			=> true,
			'ep_mask'		=> EP_PERMALINK,
		),
	);
	register_post_type( 'project', $args );
}
add_action( 'init', 'o2_register_cpt_project' );

/**
 * Registers Project category taxonomy.
 */
function o2_register_tax_project_category() {
	$labels = array(
		'name'							=> __( 'Project categories', 'o2' ),
		'singular_name'					=> __( 'Project category', 'o2' ),
		'menu_name'						=> __( 'Project categories', 'o2' ),
		'search_items'					=> __( 'Search Project categories', 'o2' ),
		'popular_items'					=> __( 'Popular Project categories', 'o2' ),
		'all_items'						=> __( 'All Project categories', 'o2' ),
		'parent_item'					=> __( 'Parent Project category', 'o2' ),
		'parent_item_colon'				=> __( 'Parent Project category:', 'o2' ),
		'edit_item'						=> __( 'Edit Project category', 'o2' ),
		'update_item'					=> __( 'Update Project category', 'o2' ),
		'add_new_item'					=> __( 'Add New Project category', 'o2' ),
		'new_item_name'					=> __( 'New Project category Name', 'o2' ),
		'add_or_remove_items'			=> __( 'Add or remove Project categories', 'o2' ),
		'choose_from_most_used'			=> __( 'Choose from most used Project categories', 'o2' ),
		'separate_items_with_commas'	=> __( 'Separate Project categories with commas', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> true,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'project-category',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'project_category', array( 'project' ), $args );
}
add_action( 'init', 'o2_register_tax_project_category' );

/**
 * Registers Project tag taxonomy.
 */
function o2_register_tax_project_tag() {
	$labels = array(
		'name'					=> __( 'Project tags', 'o2' ),
		'singular_name'			=> __( 'Project tag', 'o2' ),
		'menu_name'				=> __( 'Project tags', 'o2' ),
		'search_items'			=> __( 'Search Project tags', 'o2' ),
		'popular_items'			=> __( 'Popular Project tags', 'o2' ),
		'all_items'				=> __( 'All Project tags', 'o2' ),
		'edit_item'				=> __( 'Edit Project tag', 'o2' ),
		'update_item'			=> __( 'Update Project tag', 'o2' ),
		'add_new_item'			=> __( 'Add New Project tag', 'o2' ),
		'new_item_name'			=> __( 'New Project tag Name', 'o2' ),
		'add_or_remove_items'	=> __( 'Add or remove Project tags', 'o2' ),
		'choose_from_most_used'	=> __( 'Choose from most used Project tags', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> false,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'project-tag',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'project_tag', array( 'project' ), $args );
}
add_action( 'init', 'o2_register_tax_project_tag' );
