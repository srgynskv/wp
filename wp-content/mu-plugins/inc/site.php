<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Registers Site CPT.
 */
function o2_register_cpt_site() {
	$labels = array(
		'name'					=> __( 'Sites', 'o2' ),
		'singular_name'			=> __( 'Site', 'o2' ),
		'menu_name'				=> __( 'Sites', 'o2' ),
		'add_new'				=> __( 'Add New Site', 'o2' ),
		'add_new_item'			=> __( 'Add New Site', 'o2' ),
		'edit_item'				=> __( 'Edit Site', 'o2' ),
		'new_item'				=> __( 'New Site', 'o2' ),
		'view_item'				=> __( 'View Site', 'o2' ),
		'search_items'			=> __( 'Search Sites', 'o2' ),
		'not_found'				=> __( 'No Sites found', 'o2' ),
		'not_found_in_trash'	=> __( 'No Sites found in Trash', 'o2' ),
		'all_items'				=> __( 'All Sites', 'o2' ),
		'parent_item'			=> __( 'Parent Site', 'o2' ),
		'parent_item_colon'		=> __( 'Parent Site:', 'o2' ),
	);
	$args = array(
		'labels'				=> $labels,
		'label'					=> __( 'sites', 'o2' ),
		'description'			=> __( 'Site CPT', 'o2' ),
		'hierarchical'			=> false,
		'taxonomies'			=> array(),
		'public'				=> true,
		'publicly_queryable'	=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> null,
		'menu_icon'				=> null,
		'show_in_nav_menus'		=> false,
		'exclude_from_search'	=> false,
		'has_archive'			=> true,
		'query_var'				=> true,
		'can_export'			=> true,
		'delete_with_user'		=> false,
		'capability_type'		=> 'post',
		'supports'				=> array(
			'title', 'editor', 'excerpt', 'thumbnail',
			// 'title', 'editor', 'excerpt', 'thumbnail', 'post-formats', 'author', 'comments', 'trackbacks', 'custom-fields', 'page-attributes', 'revisions',
		),
		'rewrite' => array(
			'slug'			=> 'sites',
			'with_front'	=> false,
			'pages'			=> true,
			'feeds'			=> true,
			'ep_mask'		=> EP_PERMALINK,
		),
	);
	register_post_type( 'site', $args );
}
add_action( 'init', 'o2_register_cpt_site' );

/**
 * Registers Site category taxonomy.
 */
function o2_register_tax_site_category() {
	$labels = array(
		'name'							=> __( 'Site categories', 'o2' ),
		'singular_name'					=> __( 'Site category', 'o2' ),
		'menu_name'						=> __( 'Site categories', 'o2' ),
		'search_items'					=> __( 'Search Site categories', 'o2' ),
		'popular_items'					=> __( 'Popular Site categories', 'o2' ),
		'all_items'						=> __( 'All Site categories', 'o2' ),
		'parent_item'					=> __( 'Parent Site category', 'o2' ),
		'parent_item_colon'				=> __( 'Parent Site category:', 'o2' ),
		'edit_item'						=> __( 'Edit Site category', 'o2' ),
		'update_item'					=> __( 'Update Site category', 'o2' ),
		'add_new_item'					=> __( 'Add New Site category', 'o2' ),
		'new_item_name'					=> __( 'New Site category Name', 'o2' ),
		'add_or_remove_items'			=> __( 'Add or remove Site categories', 'o2' ),
		'choose_from_most_used'			=> __( 'Choose from most used Site categories', 'o2' ),
		'separate_items_with_commas'	=> __( 'Separate Site categories with commas', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> true,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'site-category',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'site_category', array( 'site' ), $args );
}
add_action( 'init', 'o2_register_tax_site_category' );

/**
 * Registers Site tag taxonomy.
 */
function o2_register_tax_site_tag() {
	$labels = array(
		'name'					=> __( 'Site tags', 'o2' ),
		'singular_name'			=> __( 'Site tag', 'o2' ),
		'menu_name'				=> __( 'Site tags', 'o2' ),
		'search_items'			=> __( 'Search Site tags', 'o2' ),
		'popular_items'			=> __( 'Popular Site tags', 'o2' ),
		'all_items'				=> __( 'All Site tags', 'o2' ),
		'edit_item'				=> __( 'Edit Site tag', 'o2' ),
		'update_item'			=> __( 'Update Site tag', 'o2' ),
		'add_new_item'			=> __( 'Add New Site tag', 'o2' ),
		'new_item_name'			=> __( 'New Site tag Name', 'o2' ),
		'add_or_remove_items'	=> __( 'Add or remove Site tags', 'o2' ),
		'choose_from_most_used'	=> __( 'Choose from most used Site tags', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> false,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'site-tag',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'site_tag', array( 'site' ), $args );
}
add_action( 'init', 'o2_register_tax_site_tag' );
