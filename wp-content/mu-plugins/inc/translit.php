<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

function o2_sanitize_str( $str ) {
	global $wpdb;
	$chars = array(
		'А' => 'A', 'а' => 'a',
		'Б' => 'B', 'б' => 'b',
		'В' => 'V', 'в' => 'v',
		'Г' => 'G', 'г' => 'g',
		'Д' => 'D', 'д' => 'd',
		'Е' => 'E', 'е' => 'e',
		'Ё' => 'Yo', 'ё' => 'yo',
		'Ж' => 'Zh', 'ж' => 'zh',
		'З' => 'Z', 'з' => 'z',
		'И' => 'I', 'и' => 'i',
		'Й' => 'Y', 'й' => 'y',
		'К' => 'K', 'к' => 'k',
		'Л' => 'L', 'л' => 'l',
		'М' => 'M', 'м' => 'm',
		'Н' => 'N', 'н' => 'n',
		'О' => 'O', 'о' => 'o',
		'П' => 'P', 'п' => 'p',
		'Р' => 'R', 'р' => 'r',
		'С' => 'S', 'с' => 's',
		'Т' => 'T', 'т' => 't',
		'У' => 'U', 'у' => 'u',
		'Ф' => 'F', 'ф' => 'f',
		'Х' => 'Kh', 'х' => 'kh',
		'Ц' => 'Ts', 'ц' => 'ts',
		'Ч' => 'Ch', 'ч' => 'ch',
		'Ш' => 'Sh', 'ш' => 'sh',
		'Щ' => 'Sch', 'щ' => 'sch',
		'Ъ' => '', 'ъ' => '',
		'Ы' => 'I', 'ы' => 'i',
		'Ь' => '', 'ь' => '',
		'Э' => 'E', 'э' => 'e',
		'Ю' => 'Yu', 'ю' => 'yu',
		'Я' => 'Ya', 'я' => 'ya',
		'Ґ' => 'G', 'ґ' => 'g',
		'Є' => 'Ye', 'є' => 'ye',
		'І' => 'I', 'і' => 'i',
		'Ї' => 'Yi', 'ї' => 'yi',
		'’' => '', '\'' => '',
	);
	$is_term = false;
	$backtrace = debug_backtrace();
	foreach ( $backtrace as $entry ) {
		if ( 'wp_insert_term' == $entry['function'] ) {
			$is_term = true;
			break;
		}
	}
	$term = $is_term ? $wpdb->get_var( "SELECT slug FROM {$wpdb->terms} WHERE name = '$str'" ) : '';
	if ( empty($term) ) {
		$str = strtr( $str, apply_filters( 'o2_translit_chars', $chars ) );
		if ( function_exists( 'iconv' ) ) {
			$str = iconv( 'UTF-8', 'UTF-8//TRANSLIT//IGNORE', $str );
		}
		$str = preg_replace( "/[^A-Za-z0-9_\-\.]/", '-', $str );
		$str = preg_replace( '/\-+/', '-', $str );
		$str = trim( $str, '-' );
	} else {
		$str = $term;
	}
	return $str;
}
add_filter( 'sanitize_title', 'o2_sanitize_str', 9 );
add_filter( 'sanitize_file_name', 'o2_sanitize_str' );

function o2_convert_str() {
	global $wpdb;
	$posts = $wpdb->get_results( "SELECT ID, post_name FROM {$wpdb->posts} WHERE post_name REGEXP('[^A-Za-z0-9\-]+') AND post_status IN ( 'publish', 'future', 'private' )" );
	foreach ( (array) $posts as $post ) {
		$name = o2_sanitize_str( urldecode( $post->post_name ) );
		if ( $post->post_name != $name ) {
			// add_post_meta( $post->ID, '_wp_old_slug', $post->post_name );
			$wpdb->update( $wpdb->posts, array( 'post_name' => $name ), array( 'ID' => $post->ID ) );
		}
	}
	$terms = $wpdb->get_results( "SELECT term_id, slug FROM {$wpdb->terms} WHERE slug REGEXP('[^A-Za-z0-9\-]+')" );
	foreach ( (array) $terms as $term ) {
		$slug = o2_sanitize_str( urldecode( $term->slug ) );
		if ( $term->slug != $slug ) {
			$wpdb->update( $wpdb->terms, array( 'slug' => $slug ), array( 'term_id' => $term->term_id ) );
		}
	}
}

function o2_schedule_convertion() {
	add_action( 'shutdown', 'o2_convert_str' );
}

register_activation_hook( __FILE__, 'o2_schedule_convertion' );
