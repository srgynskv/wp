<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Registers Used Product CPT.
 */
function o2_register_cpt_used_product() {
	$labels = array(
		'name'					=> __( 'Used Products', 'o2' ),
		'singular_name'			=> __( 'Used Product', 'o2' ),
		'menu_name'				=> __( 'Used Products', 'o2' ),
		'add_new'				=> __( 'Add New Used Product', 'o2' ),
		'add_new_item'			=> __( 'Add New Used Product', 'o2' ),
		'edit_item'				=> __( 'Edit Used Product', 'o2' ),
		'new_item'				=> __( 'New Used Product', 'o2' ),
		'view_item'				=> __( 'View Used Product', 'o2' ),
		'search_items'			=> __( 'Search Used Products', 'o2' ),
		'not_found'				=> __( 'No Used Products found', 'o2' ),
		'not_found_in_trash'	=> __( 'No Used Products found in Trash', 'o2' ),
		'all_items'				=> __( 'All Used Products', 'o2' ),
		'parent_item'			=> __( 'Parent Used Product', 'o2' ),
		'parent_item_colon'		=> __( 'Parent Used Product:', 'o2' ),
	);
	$args = array(
		'labels'				=> $labels,
		'label'					=> __( 'used products', 'o2' ),
		'description'			=> __( 'Used Product CPT', 'o2' ),
		'hierarchical'			=> false,
		'taxonomies'			=> array(),
		'public'				=> true,
		'publicly_queryable'	=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> null,
		'menu_icon'				=> null,
		'show_in_nav_menus'		=> false,
		'exclude_from_search'	=> false,
		'has_archive'			=> true,
		'query_var'				=> true,
		'can_export'			=> true,
		'delete_with_user'		=> false,
		'capability_type'		=> 'post',
		'register_meta_box_cb'	=> 'o2_add_meta_boxes_used_product',
		'supports'				=> array(
			'title', 'editor', 'excerpt', 'thumbnail',
			// 'title', 'editor', 'excerpt', 'thumbnail', 'post-formats', 'author', 'comments', 'trackbacks', 'custom-fields', 'page-attributes', 'revisions',
		),
		'rewrite' => array(
			'slug'			=> 'used-products',
			'with_front'	=> false,
			'pages'			=> true,
			'feeds'			=> true,
			'ep_mask'		=> EP_PERMALINK,
		),
	);
	register_post_type( 'used_product', $args );
}
add_action( 'init', 'o2_register_cpt_used_product' );

/**
 * Registers Used Product category taxonomy.
 */
function o2_register_tax_used_product_category() {
	$labels = array(
		'name'							=> __( 'Used Product categories', 'o2' ),
		'singular_name'					=> __( 'Used Product category', 'o2' ),
		'menu_name'						=> __( 'Used Product categories', 'o2' ),
		'search_items'					=> __( 'Search Used Product categories', 'o2' ),
		'popular_items'					=> __( 'Popular Used Product categories', 'o2' ),
		'all_items'						=> __( 'All Used Product categories', 'o2' ),
		'parent_item'					=> __( 'Parent Used Product category', 'o2' ),
		'parent_item_colon'				=> __( 'Parent Used Product category:', 'o2' ),
		'edit_item'						=> __( 'Edit Used Product category', 'o2' ),
		'update_item'					=> __( 'Update Used Product category', 'o2' ),
		'add_new_item'					=> __( 'Add New Used Product category', 'o2' ),
		'new_item_name'					=> __( 'New Used Product category Name', 'o2' ),
		'add_or_remove_items'			=> __( 'Add or remove Used Product categories', 'o2' ),
		'choose_from_most_used'			=> __( 'Choose from most used Used Product categories', 'o2' ),
		'separate_items_with_commas'	=> __( 'Separate Used Product categories with commas', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> true,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'used-product-category',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'used_product_category', array( 'used_product' ), $args );
}
add_action( 'init', 'o2_register_tax_used_product_category' );

/**
 * Registers Used Product tag taxonomy.
 */
function o2_register_tax_used_product_tag() {
	$labels = array(
		'name'					=> __( 'Used Product tags', 'o2' ),
		'singular_name'			=> __( 'Used Product tag', 'o2' ),
		'menu_name'				=> __( 'Used Product tags', 'o2' ),
		'search_items'			=> __( 'Search Used Product tags', 'o2' ),
		'popular_items'			=> __( 'Popular Used Product tags', 'o2' ),
		'all_items'				=> __( 'All Used Product tags', 'o2' ),
		'edit_item'				=> __( 'Edit Used Product tag', 'o2' ),
		'update_item'			=> __( 'Update Used Product tag', 'o2' ),
		'add_new_item'			=> __( 'Add New Used Product tag', 'o2' ),
		'new_item_name'			=> __( 'New Used Product tag Name', 'o2' ),
		'add_or_remove_items'	=> __( 'Add or remove Used Product tags', 'o2' ),
		'choose_from_most_used'	=> __( 'Choose from most used Used Product tags', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> false,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'used-product-tag',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'used_product_tag', array( 'used_product' ), $args );
}
add_action( 'init', 'o2_register_tax_used_product_tag' );

/**
 * Meta boxes.
 */
function o2_add_meta_boxes_used_product( $post ) {
	add_meta_box( 'o2_meta_boxes_used_product', __( 'Additional information', 'o2' ), 'o2_render_meta_boxes_used_product', $post->post_type, 'advanced', 'default', array() );
}

function o2_render_meta_boxes_used_product( $post ) {
	wp_nonce_field( 'o2_add_meta_boxes_used_product', 'o2_add_meta_boxes_used_product_nonce' );
	echo '<div class="o2-meta-box-field">'
		. '<label class="o2-meta-box-label" for="used_product_squ">' . __( 'SQU', 'o2' ) . '</label>'
		. '<input type="text" class="o2-meta-box-input" id="used_product_squ" name="used_product_squ" value="' . esc_attr( get_post_meta( $post->ID, 'used_product_squ', true ) ) . '">'
		. '</div>';
	echo '<div class="o2-meta-box-field">'
		. '<label class="o2-meta-box-label" for="used_product_price">' . __( 'Price', 'o2' ) . '</label>'
		. '<input type="text" class="o2-meta-box-input" id="used_product_price" name="used_product_price" value="' . esc_attr( get_post_meta( $post->ID, 'used_product_price', true ) ) . '">'
		. '</div>';
	echo '<div class="o2-meta-box-field">'
		. '<label class="o2-meta-box-label" for="used_product_sale_price">' . __( 'Sale price', 'o2' ) . '</label>'
		. '<input type="text" class="o2-meta-box-input" id="used_product_sale_price" name="used_product_sale_price" value="' . esc_attr( get_post_meta( $post->ID, 'used_product_sale_price', true ) ) . '">'
		. '</div>';
	echo '<div class="o2-meta-box-field">'
		. '<label class="o2-meta-box-label" for="used_product_quantity">' . __( 'Quantity', 'o2' ) . '</label>'
		. '<input type="text" class="o2-meta-box-input" id="used_product_quantity" name="used_product_quantity" value="' . esc_attr( get_post_meta( $post->ID, 'used_product_quantity', true ) ) . '">'
		. '</div>';
}

function o2_save_meta_boxes_used_product( $post_id ) {
	if ( !isset( $_POST['o2_add_meta_boxes_used_product_nonce'] )
		|| !wp_verify_nonce( $_POST['o2_add_meta_boxes_used_product_nonce'], 'o2_add_meta_boxes_used_product' )
		|| defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE
		|| !current_user_can( 'edit_post', $post_id )
		|| !isset( $_POST['used_product_squ'] )
		|| !isset( $_POST['used_product_price'] )
		|| !isset( $_POST['used_product_sale_price'] )
		|| !isset( $_POST['used_product_quantity'] ) ) {
		return;
	}
	update_post_meta( $post_id, 'used_product_squ', sanitize_text_field( $_POST['used_product_squ'] ) );
	update_post_meta( $post_id, 'used_product_price', sanitize_text_field( $_POST['used_product_price'] ) );
	update_post_meta( $post_id, 'used_product_sale_price', sanitize_text_field( $_POST['used_product_sale_price'] ) );
	update_post_meta( $post_id, 'used_product_quantity', sanitize_text_field( $_POST['used_product_quantity'] ) );
}
add_action( 'save_post', 'o2_save_meta_boxes_used_product' );
