<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Obfuscates the email address.
 */
function o2_mailto( $atts ) {
	extract( shortcode_atts( array(
		'class'	=> '',
		'url'	=> '',
		'text'	=> '',
	), $atts ) );
	if ( ! is_email( $url ) ) {
		return;
	}
	$address = '';
	$letters = str_split( trim( $url ) );
	foreach ( $letters as $letter ) {
		$address .= '&#' . ord( $letter ) . ';';
	}
	$title = $text ? $text : $address;
	return '<a class="' . $class . '" href="mailto:' . $address . '" target="_blank">' . $title . '</a>';
}
add_shortcode( 'mailto', 'o2_mailto' );
