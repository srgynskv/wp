<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Registers Product CPT.
 */
function o2_register_cpt_product() {
	$labels = array(
		'name'					=> __( 'Products', 'o2' ),
		'singular_name'			=> __( 'Product', 'o2' ),
		'menu_name'				=> __( 'Products', 'o2' ),
		'add_new'				=> __( 'Add New Product', 'o2' ),
		'add_new_item'			=> __( 'Add New Product', 'o2' ),
		'edit_item'				=> __( 'Edit Product', 'o2' ),
		'new_item'				=> __( 'New Product', 'o2' ),
		'view_item'				=> __( 'View Product', 'o2' ),
		'search_items'			=> __( 'Search Products', 'o2' ),
		'not_found'				=> __( 'No Products found', 'o2' ),
		'not_found_in_trash'	=> __( 'No Products found in Trash', 'o2' ),
		'all_items'				=> __( 'All Products', 'o2' ),
		'parent_item'			=> __( 'Parent Product', 'o2' ),
		'parent_item_colon'		=> __( 'Parent Product:', 'o2' ),
	);
	$args = array(
		'labels'				=> $labels,
		'label'					=> __( 'products', 'o2' ),
		'description'			=> __( 'Product CPT', 'o2' ),
		'hierarchical'			=> false,
		'taxonomies'			=> array(),
		'public'				=> true,
		'publicly_queryable'	=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> null,
		'menu_icon'				=> null,
		'show_in_nav_menus'		=> false,
		'exclude_from_search'	=> false,
		'has_archive'			=> true,
		'query_var'				=> true,
		'can_export'			=> true,
		'delete_with_user'		=> false,
		'capability_type'		=> 'post',
		'supports'				=> array(
			'title', 'editor', 'excerpt', 'thumbnail',
			// 'title', 'editor', 'excerpt', 'thumbnail', 'post-formats', 'author', 'comments', 'trackbacks', 'custom-fields', 'page-attributes', 'revisions',
		),
		'rewrite' => array(
			'slug'			=> 'products',
			'with_front'	=> false,
			'pages'			=> true,
			'feeds'			=> true,
			'ep_mask'		=> EP_PERMALINK,
		),
	);
	register_post_type( 'product', $args );
}
add_action( 'init', 'o2_register_cpt_product' );

/**
 * Registers Product category taxonomy.
 */
function o2_register_tax_product_category() {
	$labels = array(
		'name'							=> __( 'Product categories', 'o2' ),
		'singular_name'					=> __( 'Product category', 'o2' ),
		'menu_name'						=> __( 'Product categories', 'o2' ),
		'search_items'					=> __( 'Search Product categories', 'o2' ),
		'popular_items'					=> __( 'Popular Product categories', 'o2' ),
		'all_items'						=> __( 'All Product categories', 'o2' ),
		'parent_item'					=> __( 'Parent Product category', 'o2' ),
		'parent_item_colon'				=> __( 'Parent Product category:', 'o2' ),
		'edit_item'						=> __( 'Edit Product category', 'o2' ),
		'update_item'					=> __( 'Update Product category', 'o2' ),
		'add_new_item'					=> __( 'Add New Product category', 'o2' ),
		'new_item_name'					=> __( 'New Product category Name', 'o2' ),
		'add_or_remove_items'			=> __( 'Add or remove Product categories', 'o2' ),
		'choose_from_most_used'			=> __( 'Choose from most used Product categories', 'o2' ),
		'separate_items_with_commas'	=> __( 'Separate Product categories with commas', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> true,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'product-category',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'product_category', array( 'product' ), $args );
}
add_action( 'init', 'o2_register_tax_product_category' );

/**
 * Registers Product tag taxonomy.
 */
function o2_register_tax_product_tag() {
	$labels = array(
		'name'					=> __( 'Product tags', 'o2' ),
		'singular_name'			=> __( 'Product tag', 'o2' ),
		'menu_name'				=> __( 'Product tags', 'o2' ),
		'search_items'			=> __( 'Search Product tags', 'o2' ),
		'popular_items'			=> __( 'Popular Product tags', 'o2' ),
		'all_items'				=> __( 'All Product tags', 'o2' ),
		'edit_item'				=> __( 'Edit Product tag', 'o2' ),
		'update_item'			=> __( 'Update Product tag', 'o2' ),
		'add_new_item'			=> __( 'Add New Product tag', 'o2' ),
		'new_item_name'			=> __( 'New Product tag Name', 'o2' ),
		'add_or_remove_items'	=> __( 'Add or remove Product tags', 'o2' ),
		'choose_from_most_used'	=> __( 'Choose from most used Product tags', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> false,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'product-tag',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'product_tag', array( 'product' ), $args );
}
add_action( 'init', 'o2_register_tax_product_tag' );
