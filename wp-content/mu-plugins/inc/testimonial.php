<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Registers Testimonial CPT.
 */
function o2_register_cpt_testimonial() {
	$labels = array(
		'name'					=> __( 'Testimonials', 'o2' ),
		'singular_name'			=> __( 'Testimonial', 'o2' ),
		'menu_name'				=> __( 'Testimonials', 'o2' ),
		'add_new'				=> __( 'Add New Testimonial', 'o2' ),
		'add_new_item'			=> __( 'Add New Testimonial', 'o2' ),
		'edit_item'				=> __( 'Edit Testimonial', 'o2' ),
		'new_item'				=> __( 'New Testimonial', 'o2' ),
		'view_item'				=> __( 'View Testimonial', 'o2' ),
		'search_items'			=> __( 'Search Testimonials', 'o2' ),
		'not_found'				=> __( 'No Testimonials found', 'o2' ),
		'not_found_in_trash'	=> __( 'No Testimonials found in Trash', 'o2' ),
		'all_items'				=> __( 'All Testimonials', 'o2' ),
		'parent_item'			=> __( 'Parent Testimonial', 'o2' ),
		'parent_item_colon'		=> __( 'Parent Testimonial:', 'o2' ),
	);
	$args = array(
		'labels'				=> $labels,
		'label'					=> __( 'testimonials', 'o2' ),
		'description'			=> __( 'Testimonial CPT', 'o2' ),
		'hierarchical'			=> false,
		'taxonomies'			=> array(),
		'public'				=> true,
		'publicly_queryable'	=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> null,
		'menu_icon'				=> null,
		'show_in_nav_menus'		=> false,
		'exclude_from_search'	=> false,
		'has_archive'			=> true,
		'query_var'				=> true,
		'can_export'			=> true,
		'delete_with_user'		=> false,
		'capability_type'		=> 'post',
		'supports'				=> array(
			'title', 'editor', 'excerpt', 'thumbnail',
			// 'title', 'editor', 'excerpt', 'thumbnail', 'post-formats', 'author', 'comments', 'trackbacks', 'custom-fields', 'page-attributes', 'revisions',
		),
		'rewrite' => array(
			'slug'			=> 'testimonials',
			'with_front'	=> false,
			'pages'			=> true,
			'feeds'			=> true,
			'ep_mask'		=> EP_PERMALINK,
		),
	);
	register_post_type( 'testimonial', $args );
}
add_action( 'init', 'o2_register_cpt_testimonial' );

/**
 * Registers Testimonial category taxonomy.
 */
function o2_register_tax_testimonial_category() {
	$labels = array(
		'name'							=> __( 'Testimonial categories', 'o2' ),
		'singular_name'					=> __( 'Testimonial category', 'o2' ),
		'menu_name'						=> __( 'Testimonial categories', 'o2' ),
		'search_items'					=> __( 'Search Testimonial categories', 'o2' ),
		'popular_items'					=> __( 'Popular Testimonial categories', 'o2' ),
		'all_items'						=> __( 'All Testimonial categories', 'o2' ),
		'parent_item'					=> __( 'Parent Testimonial category', 'o2' ),
		'parent_item_colon'				=> __( 'Parent Testimonial category:', 'o2' ),
		'edit_item'						=> __( 'Edit Testimonial category', 'o2' ),
		'update_item'					=> __( 'Update Testimonial category', 'o2' ),
		'add_new_item'					=> __( 'Add New Testimonial category', 'o2' ),
		'new_item_name'					=> __( 'New Testimonial category Name', 'o2' ),
		'add_or_remove_items'			=> __( 'Add or remove Testimonial categories', 'o2' ),
		'choose_from_most_used'			=> __( 'Choose from most used Testimonial categories', 'o2' ),
		'separate_items_with_commas'	=> __( 'Separate Testimonial categories with commas', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> true,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'testimonial-category',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'testimonial_category', array( 'testimonial' ), $args );
}
add_action( 'init', 'o2_register_tax_testimonial_category' );

/**
 * Registers Testimonial tag taxonomy.
 */
function o2_register_tax_testimonial_tag() {
	$labels = array(
		'name'					=> __( 'Testimonial tags', 'o2' ),
		'singular_name'			=> __( 'Testimonial tag', 'o2' ),
		'menu_name'				=> __( 'Testimonial tags', 'o2' ),
		'search_items'			=> __( 'Search Testimonial tags', 'o2' ),
		'popular_items'			=> __( 'Popular Testimonial tags', 'o2' ),
		'all_items'				=> __( 'All Testimonial tags', 'o2' ),
		'edit_item'				=> __( 'Edit Testimonial tag', 'o2' ),
		'update_item'			=> __( 'Update Testimonial tag', 'o2' ),
		'add_new_item'			=> __( 'Add New Testimonial tag', 'o2' ),
		'new_item_name'			=> __( 'New Testimonial tag Name', 'o2' ),
		'add_or_remove_items'	=> __( 'Add or remove Testimonial tags', 'o2' ),
		'choose_from_most_used'	=> __( 'Choose from most used Testimonial tags', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> false,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'testimonial-tag',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'testimonial_tag', array( 'testimonial' ), $args );
}
add_action( 'init', 'o2_register_tax_testimonial_tag' );
