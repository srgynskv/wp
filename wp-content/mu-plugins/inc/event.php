<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Registers Event CPT.
 */
function o2_register_cpt_event() {
	$labels = array(
		'name'					=> __( 'Events', 'o2' ),
		'singular_name'			=> __( 'Event', 'o2' ),
		'menu_name'				=> __( 'Events', 'o2' ),
		'add_new'				=> __( 'Add New Event', 'o2' ),
		'add_new_item'			=> __( 'Add New Event', 'o2' ),
		'edit_item'				=> __( 'Edit Event', 'o2' ),
		'new_item'				=> __( 'New Event', 'o2' ),
		'view_item'				=> __( 'View Event', 'o2' ),
		'search_items'			=> __( 'Search Events', 'o2' ),
		'not_found'				=> __( 'No Events found', 'o2' ),
		'not_found_in_trash'	=> __( 'No Events found in Trash', 'o2' ),
		'all_items'				=> __( 'All Events', 'o2' ),
		'parent_item'			=> __( 'Parent Event', 'o2' ),
		'parent_item_colon'		=> __( 'Parent Event:', 'o2' ),
	);
	$args = array(
		'labels'				=> $labels,
		'label'					=> __( 'events', 'o2' ),
		'description'			=> __( 'Event CPT', 'o2' ),
		'hierarchical'			=> false,
		'taxonomies'			=> array(),
		'public'				=> true,
		'publicly_queryable'	=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> null,
		'menu_icon'				=> null,
		'show_in_nav_menus'		=> false,
		'exclude_from_search'	=> false,
		'has_archive'			=> true,
		'query_var'				=> true,
		'can_export'			=> true,
		'delete_with_user'		=> false,
		'capability_type'		=> 'post',
		'supports'				=> array(
			'title', 'editor', 'excerpt', 'thumbnail',
			// 'title', 'editor', 'excerpt', 'thumbnail', 'post-formats', 'author', 'comments', 'trackbacks', 'custom-fields', 'page-attributes', 'revisions',
		),
		'rewrite' => array(
			'slug'			=> 'events',
			'with_front'	=> false,
			'pages'			=> true,
			'feeds'			=> true,
			'ep_mask'		=> EP_PERMALINK,
		),
	);
	register_post_type( 'event', $args );
}
add_action( 'init', 'o2_register_cpt_event' );

/**
 * Registers Event category taxonomy.
 */
function o2_register_tax_event_category() {
	$labels = array(
		'name'							=> __( 'Event categories', 'o2' ),
		'singular_name'					=> __( 'Event category', 'o2' ),
		'menu_name'						=> __( 'Event categories', 'o2' ),
		'search_items'					=> __( 'Search Event categories', 'o2' ),
		'popular_items'					=> __( 'Popular Event categories', 'o2' ),
		'all_items'						=> __( 'All Event categories', 'o2' ),
		'parent_item'					=> __( 'Parent Event category', 'o2' ),
		'parent_item_colon'				=> __( 'Parent Event category:', 'o2' ),
		'edit_item'						=> __( 'Edit Event category', 'o2' ),
		'update_item'					=> __( 'Update Event category', 'o2' ),
		'add_new_item'					=> __( 'Add New Event category', 'o2' ),
		'new_item_name'					=> __( 'New Event category Name', 'o2' ),
		'add_or_remove_items'			=> __( 'Add or remove Event categories', 'o2' ),
		'choose_from_most_used'			=> __( 'Choose from most used Event categories', 'o2' ),
		'separate_items_with_commas'	=> __( 'Separate Event categories with commas', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> true,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'event-category',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'event_category', array( 'event' ), $args );
}
add_action( 'init', 'o2_register_tax_event_category' );

/**
 * Registers Event tag taxonomy.
 */
function o2_register_tax_event_tag() {
	$labels = array(
		'name'					=> __( 'Event tags', 'o2' ),
		'singular_name'			=> __( 'Event tag', 'o2' ),
		'menu_name'				=> __( 'Event tags', 'o2' ),
		'search_items'			=> __( 'Search Event tags', 'o2' ),
		'popular_items'			=> __( 'Popular Event tags', 'o2' ),
		'all_items'				=> __( 'All Event tags', 'o2' ),
		'edit_item'				=> __( 'Edit Event tag', 'o2' ),
		'update_item'			=> __( 'Update Event tag', 'o2' ),
		'add_new_item'			=> __( 'Add New Event tag', 'o2' ),
		'new_item_name'			=> __( 'New Event tag Name', 'o2' ),
		'add_or_remove_items'	=> __( 'Add or remove Event tags', 'o2' ),
		'choose_from_most_used'	=> __( 'Choose from most used Event tags', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> false,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'event-tag',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'event_tag', array( 'event' ), $args );
}
add_action( 'init', 'o2_register_tax_event_tag' );
