<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Registers Service CPT.
 */
function o2_register_cpt_service() {
	$labels = array(
		'name'					=> __( 'Services', 'o2' ),
		'singular_name'			=> __( 'Service', 'o2' ),
		'menu_name'				=> __( 'Services', 'o2' ),
		'add_new'				=> __( 'Add New Service', 'o2' ),
		'add_new_item'			=> __( 'Add New Service', 'o2' ),
		'edit_item'				=> __( 'Edit Service', 'o2' ),
		'new_item'				=> __( 'New Service', 'o2' ),
		'view_item'				=> __( 'View Service', 'o2' ),
		'search_items'			=> __( 'Search Services', 'o2' ),
		'not_found'				=> __( 'No Services found', 'o2' ),
		'not_found_in_trash'	=> __( 'No Services found in Trash', 'o2' ),
		'all_items'				=> __( 'All Services', 'o2' ),
		'parent_item'			=> __( 'Parent Service', 'o2' ),
		'parent_item_colon'		=> __( 'Parent Service:', 'o2' ),
	);
	$args = array(
		'labels'				=> $labels,
		'label'					=> __( 'services', 'o2' ),
		'description'			=> __( 'Service CPT', 'o2' ),
		'hierarchical'			=> false,
		'taxonomies'			=> array(),
		'public'				=> true,
		'publicly_queryable'	=> true,
		'show_ui'				=> true,
		'show_in_menu'			=> true,
		'show_in_admin_bar'		=> true,
		'menu_position'			=> null,
		'menu_icon'				=> null,
		'show_in_nav_menus'		=> false,
		'exclude_from_search'	=> false,
		'has_archive'			=> true,
		'query_var'				=> true,
		'can_export'			=> true,
		'delete_with_user'		=> false,
		'capability_type'		=> 'post',
		'supports'				=> array(
			'title', 'editor', 'excerpt', 'thumbnail',
			// 'title', 'editor', 'excerpt', 'thumbnail', 'post-formats', 'author', 'comments', 'trackbacks', 'custom-fields', 'page-attributes', 'revisions',
		),
		'rewrite' => array(
			'slug'			=> 'services',
			'with_front'	=> false,
			'pages'			=> true,
			'feeds'			=> true,
			'ep_mask'		=> EP_PERMALINK,
		),
	);
	register_post_type( 'service', $args );
}
add_action( 'init', 'o2_register_cpt_service' );

/**
 * Registers Service category taxonomy.
 */
function o2_register_tax_service_category() {
	$labels = array(
		'name'							=> __( 'Service categories', 'o2' ),
		'singular_name'					=> __( 'Service category', 'o2' ),
		'menu_name'						=> __( 'Service categories', 'o2' ),
		'search_items'					=> __( 'Search Service categories', 'o2' ),
		'popular_items'					=> __( 'Popular Service categories', 'o2' ),
		'all_items'						=> __( 'All Service categories', 'o2' ),
		'parent_item'					=> __( 'Parent Service category', 'o2' ),
		'parent_item_colon'				=> __( 'Parent Service category:', 'o2' ),
		'edit_item'						=> __( 'Edit Service category', 'o2' ),
		'update_item'					=> __( 'Update Service category', 'o2' ),
		'add_new_item'					=> __( 'Add New Service category', 'o2' ),
		'new_item_name'					=> __( 'New Service category Name', 'o2' ),
		'add_or_remove_items'			=> __( 'Add or remove Service categories', 'o2' ),
		'choose_from_most_used'			=> __( 'Choose from most used Service categories', 'o2' ),
		'separate_items_with_commas'	=> __( 'Separate Service categories with commas', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> true,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'service-category',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'service_category', array( 'service' ), $args );
}
add_action( 'init', 'o2_register_tax_service_category' );

/**
 * Registers Service tag taxonomy.
 */
function o2_register_tax_service_tag() {
	$labels = array(
		'name'					=> __( 'Service tags', 'o2' ),
		'singular_name'			=> __( 'Service tag', 'o2' ),
		'menu_name'				=> __( 'Service tags', 'o2' ),
		'search_items'			=> __( 'Search Service tags', 'o2' ),
		'popular_items'			=> __( 'Popular Service tags', 'o2' ),
		'all_items'				=> __( 'All Service tags', 'o2' ),
		'edit_item'				=> __( 'Edit Service tag', 'o2' ),
		'update_item'			=> __( 'Update Service tag', 'o2' ),
		'add_new_item'			=> __( 'Add New Service tag', 'o2' ),
		'new_item_name'			=> __( 'New Service tag Name', 'o2' ),
		'add_or_remove_items'	=> __( 'Add or remove Service tags', 'o2' ),
		'choose_from_most_used'	=> __( 'Choose from most used Service tags', 'o2' ),
	);
	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'show_in_nav_menus'	=> true,
		'show_admin_column'	=> false,
		'hierarchical'		=> false,
		'show_tagcloud'		=> true,
		'show_ui'			=> true,
		'query_var'			=> true,
		'capabilities'		=> array(),
		'rewrite' => array(
			'slug'			=> 'service-tag',
			'with_front'	=> false,
			'hierarchical'	=> false,
			'ep_mask'		=> EP_NONE,
		),
	);
	register_taxonomy( 'service_tag', array( 'service' ), $args );
}
add_action( 'init', 'o2_register_tax_service_tag' );
