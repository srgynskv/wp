<?php defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Print the variable.
 */
function o2_d( $var, $print = true, $die = true ) {
	$do = $print ? 'print_r' : 'var_dump';
	echo '<pre>';
	$do( $var );
	echo '</pre>';
	$die && die();
}

/**
 * Plugin init.
 */
function o2_init() {
	load_muplugin_textdomain( 'o2', '/languages' );
}
add_action( 'init', 'o2_init' );

/**
 * Admin CSS and JS.
 */
function o2_admin_enqueue_scripts( $hook ) {
	if ( 'edit.php' == $hook ) {
		return;
	}
	wp_enqueue_style( 'o2', O2CSS . 'o2.css' );
	wp_enqueue_script( 'o2', O2JS . 'o2.js', array(), false, true );
}
add_action( 'admin_enqueue_scripts', 'o2_admin_enqueue_scripts' );

/**
 * Rename Posts to News.
 */
function o2_change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0]					= __( 'News', 'o2' );
	$submenu['edit.php'][5][0]	= __( 'News', 'o2' );
	echo '';
}
add_action( 'admin_menu', 'o2_change_post_menu_label' );

function o2_change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name				= __( 'News', 'o2' );
	$labels->singular_name		= __( 'News', 'o2' );
	$labels->menu_name			= __( 'News', 'o2' );
	$labels->name_admin_bar		= __( 'News', 'o2' );
	$labels->add_new			= __( 'Add News', 'o2' );
	$labels->add_new_item		= __( 'Add News', 'o2' );
	$labels->edit_item			= __( 'Edit News', 'o2' );
	$labels->new_item			= __( 'New News', 'o2' );
	$labels->view_item			= __( 'View News', 'o2' );
	$labels->search_items		= __( 'Search News', 'o2' );
	$labels->not_found			= __( 'No News found', 'o2' );
	$labels->not_found_in_trash	= __( 'No News found in Trash', 'o2' );
	$labels->all_items			= __( 'All News', 'o2' );
}
add_action( 'init', 'o2_change_post_object_label' );
