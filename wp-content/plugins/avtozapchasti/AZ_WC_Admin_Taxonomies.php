<?php

/**
 * WooCommerce admin taxonomies class.
 */
$_class_wc_admin_taxonomies = WP_PLUGIN_DIR . '/woocommerce/includes/admin/class-wc-admin-taxonomies.php';

if ( file_exists( $_class_wc_admin_taxonomies ) ) {
	include_once $_class_wc_admin_taxonomies;
}

/**
 * Class AZ_WC_Admin_Taxonomies.
 */
class AZ_WC_Admin_Taxonomies extends WC_Admin_Taxonomies
{
	/**
	 * Constructor.
	 */
	public function __construct()
	{}
}

new AZ_WC_Admin_Taxonomies();
