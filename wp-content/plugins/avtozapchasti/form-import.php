<?php defined( 'ABSPATH' ) or die( '403 Forbidden' ); ?>

<div class="wrap importer">
	<header class="importer-header">
		<h1 class="importer-title">
			<span class="dashicons dashicons-index-card"></span>
			<?php _e( 'Product import from the CSV file', 'avtozapchasti' ); ?>
		</h1>

		<p class="importer-description"><?php _e( 'Choose the product category and the CSV file.', 'avtozapchasti' ); ?></p>
	</header>

	<article class="importer-article">
		<form class="importer-form" action="index.php?page=product-import" method="post" enctype="multipart/form-data" novalidate="novalidate" onsubmit="submit.disabled=true; return true;">
			<?php wp_nonce_field( 'importer_form', 'importer_form_nonce' ); ?>

			<table class="form-table">
				<tr>
					<th scope="row">
						<label for="importer-product-category"><?php _e( 'Product category', 'avtozapchasti' ); ?></label>
					</th>

					<td>
						<select name="importer-product-category" id="importer-product-category">
							<option value="">-&emsp;?&emsp;-</option>
							<option value="lubricants">Мастильні матеріали</option>
							<option value="liquids">Експлуатаційні рідини</option>
							<option value="chemicals">Автохімія</option>
							<option value="batteries">Акумуляторні батареї</option>
							<option value="spare-parts">Запчастини</option>
							<option value="filters">Фільтри</option>
						</select>
						<p class="description"></p>
					</td>
				</tr>

				<tr>
					<th scope="row">
						<label for="importer-csv-file"><?php _e( 'CSV file', 'avtozapchasti' ); ?></label>
					</th>

					<td>
						<input type="file" name="importer-csv-file" id="importer-csv-file">
						<p class="description"></p>
					</td>
				</tr>
			</table>

			<p class="alignleft submit">
				<input type="submit" class="button button-primary" name="submit" id="submit" value="<?php _e( 'Import', 'avtozapchasti' ); ?>" onclick="jQuery('.spinner').css('display', 'block');">

				<span class="spinner"></span>
			</p>

			<div class="clear"></div>
		</form>
	</article>
</div>
