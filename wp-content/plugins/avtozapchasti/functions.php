<?php

defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Print the variable.
 */
function d( $var, $print = true, $die = false ) {
	$do = $print ? 'print_r' : 'var_dump';

	echo '<pre>';
	$do( $var );
	echo '</pre>';

	$die && die();
}

/**
 * Plugin init.
 */
function avtozapchasti_init() {}

add_action( 'init', 'avtozapchasti_init' );

/**
 * Load textdomain.
 */
function avtozapchasti_load_textdomain() {
	load_plugin_textdomain( 'avtozapchasti', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}

add_action( 'plugins_loaded', 'avtozapchasti_load_textdomain' );

/**
 * Admin CSS and JS.
 */
function avtozapchasti_admin_enqueue_scripts( $hook ) {
	wp_enqueue_style( 'avtozapchasti', plugin_dir_url( __FILE__ ) . 'admin.css' );
}

add_action( 'admin_enqueue_scripts', 'avtozapchasti_admin_enqueue_scripts' );

/**
 * Rename Posts to News.
 */
function avtozapchasti_change_post_menu_label() {
	global $menu;
	global $submenu;

	$menu[5][0]					= __( 'News', 'avtozapchasti' );
	$submenu['edit.php'][5][0]	= __( 'News', 'avtozapchasti' );

	echo '';
}

add_action( 'admin_menu', 'avtozapchasti_change_post_menu_label' );

function avtozapchasti_change_post_object_label() {
	global $wp_post_types;

	$labels = &$wp_post_types['post']->labels;
	$labels->name				= __( 'News', 'avtozapchasti' );
	$labels->singular_name		= __( 'News', 'avtozapchasti' );
	$labels->menu_name			= __( 'News', 'avtozapchasti' );
	$labels->name_admin_bar		= __( 'News', 'avtozapchasti' );
	$labels->add_new			= __( 'Add News', 'avtozapchasti' );
	$labels->add_new_item		= __( 'Add News', 'avtozapchasti' );
	$labels->edit_item			= __( 'Edit News', 'avtozapchasti' );
	$labels->new_item			= __( 'New News', 'avtozapchasti' );
	$labels->view_item			= __( 'View News', 'avtozapchasti' );
	$labels->search_items		= __( 'Search News', 'avtozapchasti' );
	$labels->not_found			= __( 'No News found', 'avtozapchasti' );
	$labels->not_found_in_trash	= __( 'No News found in Trash', 'avtozapchasti' );
	$labels->all_items			= __( 'All News', 'avtozapchasti' );
}

add_action( 'init', 'avtozapchasti_change_post_object_label' );

/**
 * Sanitizes the string.
 */
function avtozapchasti_sanitize_str( $str ) {
	global $wpdb;

	$chars = array(
		'А' => 'A', 'а' => 'a',
		'Б' => 'B', 'б' => 'b',
		'В' => 'V', 'в' => 'v',
		'Г' => 'G', 'г' => 'g',
		'Д' => 'D', 'д' => 'd',
		'Е' => 'E', 'е' => 'e',
		'Ё' => 'Yo', 'ё' => 'yo',
		'Ж' => 'Zh', 'ж' => 'zh',
		'З' => 'Z', 'з' => 'z',
		'И' => 'I', 'и' => 'i',
		'Й' => 'Y', 'й' => 'y',
		'К' => 'K', 'к' => 'k',
		'Л' => 'L', 'л' => 'l',
		'М' => 'M', 'м' => 'm',
		'Н' => 'N', 'н' => 'n',
		'О' => 'O', 'о' => 'o',
		'П' => 'P', 'п' => 'p',
		'Р' => 'R', 'р' => 'r',
		'С' => 'S', 'с' => 's',
		'Т' => 'T', 'т' => 't',
		'У' => 'U', 'у' => 'u',
		'Ф' => 'F', 'ф' => 'f',
		'Х' => 'Kh', 'х' => 'kh',
		'Ц' => 'Ts', 'ц' => 'ts',
		'Ч' => 'Ch', 'ч' => 'ch',
		'Ш' => 'Sh', 'ш' => 'sh',
		'Щ' => 'Sch', 'щ' => 'sch',
		'Ъ' => '', 'ъ' => '',
		'Ы' => 'I', 'ы' => 'i',
		'Ь' => '', 'ь' => '',
		'Э' => 'E', 'э' => 'e',
		'Ю' => 'Yu', 'ю' => 'yu',
		'Я' => 'Ya', 'я' => 'ya',
		'Ґ' => 'G', 'ґ' => 'g',
		'Є' => 'Ye', 'є' => 'ye',
		'І' => 'I', 'і' => 'i',
		'Ї' => 'Yi', 'ї' => 'yi',
		'’' => '', '\'' => '',
		'.' => '-', ',' => '-',
	);

	$is_term = false;
	$backtrace = debug_backtrace();

	foreach ( $backtrace as $entry ) {
		if ( 'wp_insert_term' == $entry['function'] ) {
			$is_term = true;
			break;
		}
	}

	$term = $is_term ? $wpdb->get_var( "SELECT slug FROM {$wpdb->terms} WHERE name = '$str'" ) : '';

	if ( empty($term) ) {
		$str = strtr( $str, apply_filters( 'avtozapchasti_translit_chars', $chars ) );

		if ( function_exists( 'iconv' ) ) {
			$str = iconv( 'UTF-8', 'UTF-8//TRANSLIT//IGNORE', $str );
		}

		$str = preg_replace( "/[^A-Za-z0-9_\-\.]/", '-', $str );
		$str = preg_replace( '/\-+/', '-', $str );
		$str = trim( $str, '-' );
	} else {
		$str = $term;
	}

	return strtolower( $str );
}

add_filter( 'sanitize_title', 'avtozapchasti_sanitize_str', 9 );
add_filter( 'sanitize_file_name', 'avtozapchasti_sanitize_str' );

function avtozapchasti_convert_str() {
	global $wpdb;

	$posts = $wpdb->get_results( "SELECT ID, post_name FROM {$wpdb->posts} WHERE post_name REGEXP('[^A-Za-z0-9\-]+') AND post_status IN ( 'publish', 'future', 'private' )" );

	foreach ( (array) $posts as $post ) {
		$name = avtozapchasti_sanitize_str( urldecode( $post->post_name ) );

		if ( $post->post_name != $name ) {
			// add_post_meta( $post->ID, '_wp_old_slug', $post->post_name );
			$wpdb->update( $wpdb->posts, array( 'post_name' => $name ), array( 'ID' => $post->ID ) );
		}
	}

	$terms = $wpdb->get_results( "SELECT term_id, slug FROM {$wpdb->terms} WHERE slug REGEXP('[^A-Za-z0-9\-]+')" );

	foreach ( (array) $terms as $term ) {
		$slug = avtozapchasti_sanitize_str( urldecode( $term->slug ) );

		if ( $term->slug != $slug ) {
			$wpdb->update( $wpdb->terms, array( 'slug' => $slug ), array( 'term_id' => $term->term_id ) );
		}
	}
}

function avtozapchasti_schedule_convertion() {
	add_action( 'shutdown', 'avtozapchasti_convert_str' );
}

register_activation_hook( __FILE__, 'avtozapchasti_schedule_convertion' );

/**
 * In multi array?
 */
function avtozapchasti_in_multi_array( $needle, $haystack, $strict = false ) {
	foreach ( $haystack as $item ) {
		if ( ( $strict ? $item === $needle : $item == $needle )
			|| ( is_array( $item )
				&& avtozapchasti_in_multi_array( $needle, $item, $strict ) ) ) {
			return $item;
		}
	}

	return false;
}

/**
 * Obfuscates the email address.
 */
function avtozapchasti_mailto( $atts ) {
	extract( shortcode_atts( array(
		'class'	=> '',
		'url'	=> '',
		'text'	=> '',
	), $atts ) );

	if ( ! is_email( $url ) ) {
		return;
	}

	$address = '';
	$letters = str_split( trim( $url ) );

	foreach ( $letters as $letter ) {
		$address .= '&#' . ord( $letter ) . ';';
	}

	$title = $text ? $text : $address;

	return '<a class="' . $class . '" href="mailto:' . $address . '" target="_blank">' . $title . '</a>';
}

add_shortcode( 'mailto', 'avtozapchasti_mailto' );
