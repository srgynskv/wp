<?php
/**
 * Plugin Name: AvtoZapchasti
 * Description: Advanced functionality for AvtoZapchasti store.
 * Plugin URI:
 * Author: Serge Noskov
 * Author URI: http://noskov.biz/
 * Version: 1.0
 * Text Domain: avtozapchasti
 * Domain Path: /languages/
 */

defined( 'ABSPATH' ) or die( '403 Forbidden' );

// Functions
include_once plugin_dir_path( __FILE__ ) . 'functions.php';

// Settings
include_once plugin_dir_path( __FILE__ ) . 'settings.php';

// WooCommerce
include_once plugin_dir_path( __FILE__ ) . 'AZ_WC_Admin_Taxonomies.php';
include_once plugin_dir_path( __FILE__ ) . 'woocommerce.php';

// Product import
include_once plugin_dir_path( __FILE__ ) . 'import.php';
