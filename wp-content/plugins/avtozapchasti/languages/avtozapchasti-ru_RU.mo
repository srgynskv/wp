��          �      |      �  >   �     0     9     B     P     g  -   p     �  A   �  	   �     �     �                    2     C      R     s       	   �  �  �  b   P     �     �     �  <   �     :  C   G     �  ]   �     �          8     X  "   g  4   �  !   �     �  /   �     -	  !   I	  %   k	                            
   	                                                                  <p>Updated: <b>%d</b> products. Added: <b>%d</b> products.</p> Add News All News Avtozapchasti Avtozapchasti settings CSV file Choose the product category and the CSV file. Currency rate Define your actual currency rate (USD - UAH). For example, 20.70. Edit News Import New News News No News found No News found in Trash Product category Product import Product import from the CSV file Search News Ukrainian hryvnia View News Project-Id-Version: AvtoZapchasti v1.0
POT-Creation-Date: 2015-02-12 10:18+0300
PO-Revision-Date: 2015-02-12 10:18+0300
Last-Translator: Serge Noskov <serge@noskov.biz>
Language-Team: Serge Noskov <serge@noskov.biz>
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: __;_e;_n;_x;_ex;_nx;_n_noop;_nx_noop;__ngettext:1,2
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=4; plural=(n==1) ? 0 : (n%10==1 && n%100!=11) ? 3 : ((n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20)) ? 1 : 2);
X-Poedit-SearchPath-0: /home/serge/www/bogdan/site/wp-content/plugins/avtozapchasti
 <p>Обновлено: <b>%d</b> товаров. Добавлено: <b>%d</b> товаров.</p> Добавить новость Все новости Курс валют Настройки плагина "Автозапчасти" CSV файл Выберите категорию товаров и CSV файл. Курс валют Укажите актуальный курс валют (USD - UAH). Например, 20.70. Изменить новость Импортировать Добавить новость Новости Новости не найдены В корзине новости не найдены Категория товаров Импорт товаров Импорт товаров из CSV файла Поиск новостей Украинская гривна Просмотреть новость 