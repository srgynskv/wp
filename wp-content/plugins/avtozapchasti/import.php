<?php

defined( 'ABSPATH' ) or die( '403 Forbidden' );

/**
 * Add dashboard menu item.
 */
function avtozapchasti_add_dashboard_page() {
	add_dashboard_page(
		__( 'Product import from the CSV file', 'avtozapchasti' ),
		__( 'Product import', 'avtozapchasti' ),
		'import',
		'product-import',
		'avtozapchasti_form_import'
	);
}

// add_action( 'admin_menu', 'avtozapchasti_add_dashboard_page' );

/**
 * Display import form.
 */
function avtozapchasti_form_import() {
	require_once plugin_dir_path( __FILE__ ) . 'form-import.php';

	if ( avtozapchasti_do_import() ) {
		return;
	}

	if ( empty( $_POST['importer_form_nonce'] )
		|| ! check_admin_referer( 'importer_form', 'importer_form_nonce' ) ) {
		return;
	}
}

function avtozapchasti_do_import() {
	if ( empty( $_POST['importer-product-category'] )
		|| empty( $_FILES['importer-csv-file'] )
		|| 'text/csv' !== $_FILES['importer-csv-file']['type'] ) {
		return;
	}

	$taxonomy = 'product_cat';
	$parent = get_term_by( 'slug', $_POST['importer-product-category'], $taxonomy, OBJECT );
	$grand_parent = get_term( $parent->parent, $taxonomy );
	$terms = array( (int) $grand_parent->term_id, (int) $parent->term_id );

	$fileinfo = $_FILES['importer-csv-file'];
	$filename = avtozapchasti_upload_csv_file( $fileinfo, $parent );
	$new = avtozapchasti_get_new_products( $filename );
	$old_categories = get_terms( $taxonomy, array(
		'hide_empty' => false,
		'parent' => $parent->term_id,
	) );
	$old_products = avtozapchasti_get_old_products( $parent );

	$updated = $created = 0;
	avtozapchasti_update_categories( $new['categories'], $old_categories, $parent, $taxonomy );

	foreach ( $new['products'] as $new_product ) {
		$old_product = avtozapchasti_in_multi_array( $new_product[3], $old_products );

		if ( ! empty( $old_product ) ) {
			avtozapchasti_update_product( $new_product, $old_product, $terms, $taxonomy );
			$updated++;
		} else {
			avtozapchasti_create_product( $new_product, $terms, $taxonomy );
			$created++;
		}
	}

	echo '<div class="updated importer-statistics">'
		. sprintf( __( '<p>Updated: <b>%d</b> products. Added: <b>%d</b> products.</p>', 'avtozapchasti' ), $updated, $created )
		. '</div>';
}

/**
 * Upload a file.
 */
function avtozapchasti_upload_csv_file( $fileinfo, $parent ) {
	$upload_dir = wp_upload_dir();
	$directory = $upload_dir['basedir'] . '/csv-files/';
	is_dir( $directory ) || @mkdir( $directory );
	$destination = $directory . $parent->slug . '-' . date( 'Y-m-d' ) . '.csv';

	move_uploaded_file( $fileinfo['tmp_name'], $destination );

	$content_raw = file_get_contents( $destination );
	$content_utf = mb_convert_encoding( $content_raw, 'UTF-8', 'Windows-1251' );
	$content_utf = trim( $content_utf, " \n\t" );

	file_put_contents( $destination, $content_utf );

	return $destination;
}

/**
 * Get new products.
 */
function avtozapchasti_get_new_products( $filename ) {
	$categories = $products = array();
	$content = file_get_contents( $filename );
	$strings = explode( "\n", $content );

	foreach ( $strings as $key => $value ) {
		$products[$key] = explode( ';', $value );
		$products[$key][0] = trim( $products[$key][0], ' ="' );
		$products[$key][0] = empty( $products[$key][0] )
			? 'AZ-' . microtime( true )
			: $products[$key][0];
		$products[$key][1] = trim( $products[$key][1] );
		$products[$key][3] = trim( preg_replace( array( '/\s\s+/', '/""+/' ), array( ' ', '"' ), $products[$key][3] ) );
		$products[$key][4] = trim( $products[$key][4], ' ="><' );
		$products[$key][6] = trim( $products[$key][6], ' ="' );
	}

	foreach ( $products as $key => $product ) {
		$categories[] = $product[1];
	}

	$categories = array_unique( $categories );

	return array( 'products' => $products, 'categories' => $categories );
}

/**
 * Get old products.
 */
function avtozapchasti_get_old_products( $parent ) {
	$args = array(
		'post_type' => 'product',
		'post_status' => 'any',
		'tax_query' => array(
			array(
				'taxonomy' => 'product_cat',
				'field' => 'id',
				'terms' => array( $parent->term_id ),
				'include_children' => true,
				'operator' => 'IN',
			),
		),
		'meta_query' => array(
			array(
				'key' => '_sku',
				'compare' => 'EXISTS',
			),
		),
		'nopaging' => true,
		'posts_per_page' => -1,
		'order' => 'ASC',
		'orderby' => 'meta_value',
		'no_found_rows' => false,
		'cache_results' => true,
		'update_post_term_cache' => true,
		'update_post_meta_cache' => true,
	);
	$query = new WP_Query( $args );
	$products = $query->posts;

	foreach ( $products as $key => $product ) {
		$products[$key]->sku = get_post_meta( $product->ID, '_sku', true );
		is_array( $products[$key] ) || $products[$key] = (array) $products[$key];
	}

	return $products;
}

/**
 * Update categories.
 */
function avtozapchasti_update_categories( $new_categories, $old_categories, $parent, $taxonomy ) {
	if ( empty( $new_categories ) ) {
		return;
	}

	foreach ( $new_categories as $new_category ) {
		$old_category = avtozapchasti_in_multi_array( $new_category, $old_categories );

		if ( ! empty( $old_category ) ) {
			wp_update_term( $old_category->term_id, $taxonomy, array(
				'name' => $new_category,
				'slug' => avtozapchasti_sanitize_str( $new_category ),
				'parent' => $parent->term_id,
			) );
		} else {
			wp_insert_term( $new_category, $taxonomy, array(
				'name' => $new_category,
				'slug' => avtozapchasti_sanitize_str( $new_category ),
				'parent' => $parent->term_id,
			) );
		}
	}
}

/**
 * Update the product.
 */
function avtozapchasti_update_product( $new_product, $old_product, $terms, $taxonomy ) {
	global $user_ID;

	$postarr = array(
		'ID' => $old_product['ID'],
		'post_author' => $user_ID,
		'post_title' => $new_product[3],
		'post_status' => 'publish',
		'comment_status' => 'closed',
		'ping_status' => 'closed',
		'post_name' => avtozapchasti_sanitize_str( $new_product[3] ),
		'post_modified' => current_time( 'mysql' ),
		'post_modified_gmt' => current_time( 'mysql', true ),
	);
	$product_id = wp_update_post( $postarr );

	if ( ! $product_id ) {
		return;
	}

	update_post_meta( $product_id, '_sku', $new_product[0] );
	update_post_meta( $product_id, '_stock', $new_product[4] );
	update_post_meta( $product_id, '_regular_price', $new_product[6] );
	update_post_meta( $product_id, '_price', $new_product[6] );
	update_post_meta( $product_id, '_edit_last', $user_ID );
	update_post_meta( $product_id, '_edit_lock', time() . ':' . $user_ID );

	$term = get_term_by( 'name', $new_product[1], $taxonomy, ARRAY_A );

	if ( ! empty( $term ) ) {
		$terms[] = (int) $term['term_id'];
	}

	wp_set_object_terms( $product_id, $terms, $taxonomy, false );
}

/**
 * Create a product.
 */
function avtozapchasti_create_product( $new_product, $terms, $taxonomy ) {
	global $user_ID;

	$postarr = array(
		'post_author' => $user_ID,
		'post_date' => current_time( 'mysql' ),
		'post_date_gmt' => current_time( 'mysql', 1 ),
		'post_content' => '',
		'post_title' => $new_product[3],
		'post_excerpt' => '',
		'post_status' => 'publish',
		'comment_status' => 'closed',
		'ping_status' => 'closed',
		'post_password' => '',
		'post_name' => avtozapchasti_sanitize_str( $new_product[3] ),
		'to_ping' => '',
		'pinged' => '',
		'post_modified' => current_time( 'mysql' ),
		'post_modified_gmt' => current_time( 'mysql', 1 ),
		'post_content_filtered' => '',
		'post_parent' => 0,
		'guid' => '',
		'menu_order' => 0,
		'post_type' => 'product',
		'post_mime_type' => '',
		'comment_count' => 0,
	);
	$product_id = wp_insert_post( $postarr );

	if ( ! $product_id ) {
		return;
	}

	update_post_meta( $product_id, '_edit_last', $user_ID );
	update_post_meta( $product_id, '_edit_lock', time() . ':' . $user_ID );
	update_post_meta( $product_id, '_thumbnail_id', '' );
	update_post_meta( $product_id, '_visibility', 'visible' );
	update_post_meta( $product_id, '_stock_status', 'instock' );
	update_post_meta( $product_id, 'total_sales', 0 );
	update_post_meta( $product_id, '_downloadable', 'no' );
	update_post_meta( $product_id, '_virtual', 'no' );
	update_post_meta( $product_id, '_product_image_gallery', '' );
	update_post_meta( $product_id, '_regular_price', $new_product[6] );
	update_post_meta( $product_id, '_sale_price', '' );
	update_post_meta( $product_id, '_tax_status', '' );
	update_post_meta( $product_id, '_tax_class', '' );
	update_post_meta( $product_id, '_purchase_note', '' );
	update_post_meta( $product_id, '_featured', 'no' );
	update_post_meta( $product_id, '_weight', '' );
	update_post_meta( $product_id, '_length', '' );
	update_post_meta( $product_id, '_width', '' );
	update_post_meta( $product_id, '_height', '' );
	update_post_meta( $product_id, '_sku', $new_product[0] );
	update_post_meta( $product_id, '_product_attributes', 'a:0:{}' );
	update_post_meta( $product_id, '_sale_price_dates_from', '' );
	update_post_meta( $product_id, '_sale_price_dates_to', '' );
	update_post_meta( $product_id, '_price', $new_product[6] );
	update_post_meta( $product_id, '_sold_individually', '' );
	update_post_meta( $product_id, '_stock', $new_product[4] );
	update_post_meta( $product_id, '_backorders', 'no' );
	update_post_meta( $product_id, '_manage_stock', 'yes' );
	update_post_meta( $product_id, '_upsell_ids', '' );

	$term = get_term_by( 'name', $new_product[1], $taxonomy, ARRAY_A );

	if ( ! empty( $term ) ) {
		$terms[] = (int) $term['term_id'];
	}

	wp_set_object_terms( $product_id, $terms, $taxonomy, false );
}
