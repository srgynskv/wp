<?php

/**
 * Avtozapchasti plugin settings page.
 *
 * Adds global extra charge.
 * Further it will be used to convert raw product prices with extra charge.
 */
defined( 'ABSPATH' ) or die( '403 Forbidden' );

function avtozapchasti_add_admin_menu() {
	add_options_page(
		__( 'Общая наценка', 'avtozapchasti' ),
		__( 'Общая наценка', 'avtozapchasti' ),
		'manage_options',
		'avtozapchasti',
		'avtozapchasti_options_page'
	);
}

add_action( 'admin_menu', 'avtozapchasti_add_admin_menu' );

function avtozapchasti_settings_init() {
	register_setting( 'avtozapchasti', 'avtozapchasti_settings' );

	add_settings_section(
		'avtozapchasti_avtozapchasti_section',
		__( 'Настройки плагина', 'avtozapchasti' ),
		'avtozapchasti_settings_section_callback',
		'avtozapchasti'
	);

	add_settings_field(
		'avtozapchasti_global_extra_charge',
		__( 'Наценка', 'avtozapchasti' ),
		'avtozapchasti_global_extra_charge_render',
		'avtozapchasti',
		'avtozapchasti_avtozapchasti_section'
	);
}

add_action( 'admin_init', 'avtozapchasti_settings_init' );

function avtozapchasti_global_extra_charge_render() {
	$options = get_option( 'avtozapchasti_settings' ); ?>
	<input type='text' name='avtozapchasti_settings[avtozapchasti_global_extra_charge]' value='<?php echo $options['avtozapchasti_global_extra_charge']; ?>'>
<?php }

function avtozapchasti_settings_section_callback() {
	echo __( 'Укажите коэффициент наценки. Например, 1.3 или 0.7.', 'avtozapchasti' );
}

function avtozapchasti_options_page() { ?>
	<form action='options.php' method='post'>
		<?php
			settings_fields( 'avtozapchasti' );
			do_settings_sections( 'avtozapchasti' );
			submit_button();
		?>
	</form>
<?php }
