<?php

/**
 * Add Ukrainian hryvnia.
 */
function avtozapchasti_currency_uah( $currencies ) {
	$currencies['UAH'] = __( 'Ukrainian hryvnia', 'avtozapchasti' );

	return $currencies;
}

add_filter( 'woocommerce_currencies', 'avtozapchasti_currency_uah' );

/**
 * Add Ukrainian hryvnia sign.
 */
function avtozapchasti_currency_uah_symbol( $currency_symbol, $currency ) {
	switch( $currency ) {
		case 'UAH':
		$currency_symbol = '&#8372;'; // &#8372; or ₴
		break;
	}

	return $currency_symbol;
}

add_filter( 'woocommerce_currency_symbol', 'avtozapchasti_currency_uah_symbol', 10, 2 );

/**
 * Change raw price. Convert it into Ukrainian hryvnia (from USD).
 */
function avtozapchasti_uah_price( $price ) {
	$settings = get_option( 'avtozapchasti_settings' );
	$currency_rate = floatval( str_replace( ',', '.', $settings['avtozapchasti_currency_rate'] ) );

	if ( ! empty( $currency_rate ) ) {
		$price = floatval( $price * $currency_rate );
	}

	return $price;
}

add_filter( 'raw_woocommerce_price', 'avtozapchasti_uah_price', 10, 1 );
