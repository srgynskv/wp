<?php

die();

/**
 * Every time you need to change:
 * 1. file to upload
 * 2. product ID
 * 3. category ID
 * 4. parent cat ID
 * 5. vendor ID
 */

/**
 * Database settings
 */
$hostname	= 'localhost';
$database	= 'bogdan_avtozap';
$username	= 'root';
$password	= '';

// files
$file_posts					= __DIR__ . '/csv-got/posts.csv';
$file_postmeta				= __DIR__ . '/csv-got/postmeta.csv';
$file_term_taxonomy			= __DIR__ . '/csv-got/term_taxonomy.csv';
$file_terms					= __DIR__ . '/csv-got/terms.csv';
$file_term_relationships	= __DIR__ . '/csv-got/term_relationships.csv';

try {
	// db connection
	$conn = new PDO('mysql:host=' . $hostname . ';dbname=' . $database, $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo '<p>Connected successfully.</p>';

	// create new files from arrays
	create_arrays();

	// posts
	$conn->query("LOAD DATA INFILE '$file_posts'
		REPLACE
		INTO TABLE f3jh7_posts
		CHARACTER SET utf8
		FIELDS TERMINATED BY ';'
		(@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9, @col10, @col11)
		SET ID = @col1,
			post_author = 2,
			post_date = '2015-03-27 07:03:37',
			post_date_gmt = '2015-03-27 07:03:37',
			post_title = @col2,
			ping_status = 'closed',
			post_name = @col3,
			post_modified = '2015-03-27 07:03:37',
			post_modified_gmt = '2015-03-27 07:03:37',
			guid = CONCAT('http://avtozap.kiev.ua/?p=', @col1),
			post_type = 'product'
	;");

	// postmeta
	$conn->query("LOAD DATA INFILE '$file_postmeta'
		REPLACE
		INTO TABLE f3jh7_postmeta
		CHARACTER SET utf8
		FIELDS TERMINATED BY ';'
		(@col1, @col2, @col3)
		SET post_id = @col1,
			meta_key = @col2,
			meta_value = @col3
	;");

	// term_taxonomy
	$conn->query("LOAD DATA INFILE '$file_term_taxonomy'
		REPLACE
		INTO TABLE f3jh7_term_taxonomy
		CHARACTER SET utf8
		FIELDS TERMINATED BY ';'
		(@col1, @col2, @col3, @col4)
		SET term_taxonomy_id = @col1,
			term_id = @col1,
			taxonomy = 'product_cat',
			parent = @col2
	;");

	// terms
	$conn->query("LOAD DATA INFILE '$file_terms'
		REPLACE
		INTO TABLE f3jh7_terms
		CHARACTER SET utf8
		FIELDS TERMINATED BY ';'
		(@col1, @col2, @col3)
		SET term_id = @col1,
			name = @col2,
			slug = @col3
	;");

	// term_relationships
	$conn->query("LOAD DATA INFILE '$file_term_relationships'
		REPLACE
		INTO TABLE f3jh7_term_relationships
		CHARACTER SET utf8
		FIELDS TERMINATED BY ';'
		(@col1, @col2)
		SET object_id = @col1,
			term_taxonomy_id = @col2
	;");
} catch(PDOException $e) {
	echo 'Connection failed: ' . $e->getMessage();
}

function create_arrays() {
	/**
	 * CHANGE THIS!
	 */
	$pre_id		= 214001;
	$pre_tid	= 1651;
	$pre_par	= 114;
	$pre_ven	= 114;
	$source		= 'elit';

	// all raw products
	$all = file_get_contents(__DIR__ . '/csv-got/' . $source . '.csv');

	// lines
	$lines = explode("\n", $all);

	unset($all);

	// columns
	foreach ($lines as $key => $line) {
		$column = explode(';', $line);

		if (!empty($column[1])) {
			$columns[] = $column;
		}
	}

	unset($lines);

	foreach ($columns as $key => $col) {
		// sku
		$col[0] = trim($col[0], ' ="><+!');
		$col[0] = empty($col[0]) ? 'AZ-' . microtime(true) : $col[0];

		// title
		$col[1] = trim(preg_replace(
			array('/\s\s+/', '/""+/', '/=+/', '/!+/'),
			array(' ', '"', '', ''),
			$col[1]),
		' ="><+!');
		$col[1] = empty($col[1]) ? 'АвтоЗапчасти ' . microtime(true) : $col[1];

		// category/brand
		$col[3] = trim(preg_replace(
			array('/\s\s+/', '/""+/', '/=+/', '/!+/'),
			array(' ', '"', '', ''),
			$col[3]),
		' ="><+!');
		$col[3] = empty($col[3]) ? 'AVTOZAP' : $col[3];

		// price
		$col[4] = trim($col[4], ' ="><+!');

		// stock
		$col[5] = trim($col[5], ' ="><+!');
		$col[5] = empty($col[5]) ? '3' : $col[5];


		// posts
		$posts[] = $pre_id + $key . ';' . $col[1] . ';' . sanitize_str($col[1]);

		// postmeta
		$postmeta[] = $pre_id + $key . ';' . '_sku' . ';' . $col[0];
		$postmeta[] = $pre_id + $key . ';' . '_visibility' . ';' . 'visible';
		$postmeta[] = $pre_id + $key . ';' . '_manage_stock' . ';' . 'yes';
		$postmeta[] = $pre_id + $key . ';' . '_stock_status' . ';' . 'instock';
		$postmeta[] = $pre_id + $key . ';' . '_stock' . ';' . $col[5];
		$postmeta[] = $pre_id + $key . ';' . '_regular_price' . ';' . $col[4];
		$postmeta[] = $pre_id + $key . ';' . '_price' . ';' . $col[4];

		// terms
		$terms[]	= $col[3] . ';' . sanitize_str($col[3]);
		$brands[]	= $col[3];
	}

	// terms & brands
	$terms	= array_values(array_unique($terms));
	$brands	= array_values(array_unique($brands));

	// term_taxonomy
	foreach ($terms as $key => $term) {
		$terms[$key] = $pre_tid + $key . ';' . $term;
		$term_taxonomy[] = $pre_tid + $key . ';' . $pre_par;
	}

	foreach ($columns as $colkey => $colval) {
		$tid = $pre_tid + array_search($colval[3], $brands);

		// new
		$term_relationships[] = $pre_id + $colkey . ';' . '104';
		// parent cat
		// $term_relationships[] = $pre_id + $colkey . ';' . $pre_par;
		// vendor
		$term_relationships[] = $pre_id + $colkey . ';' . $pre_ven;
		// cat
		$term_relationships[] = $pre_id + $colkey . ';' . $tid;

		unset($tid);
	}

	unset($columns, $brands);

	// files
	file_put_contents(__DIR__ . '/csv-got/posts.csv', implode("\n", $posts));
	file_put_contents(__DIR__ . '/csv-got/postmeta.csv', implode("\n", $postmeta));
	file_put_contents(__DIR__ . '/csv-got/term_taxonomy.csv', implode("\n", $term_taxonomy));
	file_put_contents(__DIR__ . '/csv-got/terms.csv', implode("\n", $terms));
	file_put_contents(__DIR__ . '/csv-got/term_relationships.csv', implode("\n", $term_relationships));

	unset($posts, $postmeta, $term_taxonomy, $terms, $term_relationships);
}

/**
 * Dump a variable.
 */
function d( $var, $print = true, $die = true ) {
	$do = $print ? 'print_r' : 'var_dump';
	echo '<pre>';
	$do( $var );
	echo '</pre>';
	$die && die();
}

/**
 * Sanitizes the string.
 */
function sanitize_str( $str ) {
	$chars = array(
		'А' => 'A', 'а' => 'a', 'Б' => 'B', 'б' => 'b', 'В' => 'V', 'в' => 'v', 'Г' => 'G', 'г' => 'g', 'Д' => 'D', 'д' => 'd', 'Е' => 'E', 'е' => 'e', 'Ё' => 'Yo', 'ё' => 'yo', 'Ж' => 'Zh', 'ж' => 'zh', 'З' => 'Z', 'з' => 'z', 'И' => 'I', 'и' => 'i', 'Й' => 'Y', 'й' => 'y', 'К' => 'K', 'к' => 'k', 'Л' => 'L', 'л' => 'l', 'М' => 'M', 'м' => 'm', 'Н' => 'N', 'н' => 'n', 'О' => 'O', 'о' => 'o', 'П' => 'P', 'п' => 'p', 'Р' => 'R', 'р' => 'r', 'С' => 'S', 'с' => 's', 'Т' => 'T', 'т' => 't', 'У' => 'U', 'у' => 'u', 'Ф' => 'F', 'ф' => 'f', 'Х' => 'Kh', 'х' => 'kh', 'Ц' => 'Ts', 'ц' => 'ts', 'Ч' => 'Ch', 'ч' => 'ch', 'Ш' => 'Sh', 'ш' => 'sh', 'Щ' => 'Sch', 'щ' => 'sch', 'Ъ' => '', 'ъ' => '', 'Ы' => 'I', 'ы' => 'i', 'Ь' => '', 'ь' => '', 'Э' => 'E', 'э' => 'e', 'Ю' => 'Yu', 'ю' => 'yu', 'Я' => 'Ya', 'я' => 'ya', 'Ґ' => 'G', 'ґ' => 'g', 'Є' => 'Ye', 'є' => 'ye', 'І' => 'I', 'і' => 'i', 'Ї' => 'Yi', 'ї' => 'yi', '’' => '', '\'' => '', '.' => '-', ',' => '-',
	);

	$res = strtr($str, $chars);
	$res = preg_replace( "/[^A-Za-z0-9_\-\.]/", '-', $res );
	$res = preg_replace( '/\-+/', '-', $res );
	$res = trim( $res, '-' );

	return strtolower($res);
}
