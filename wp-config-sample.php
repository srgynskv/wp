<?php

// define( 'RELOCATE', true );
// define( 'WP_MEMORY_LIMIT', '96M' );

// define( 'WP_CACHE', true );
// define( 'WPCACHEHOME', dirname( __FILE__ ) . '/wp-content/plugins/wp-super-cache/' );

define( 'WP_DEFAULT_THEME', 'twentyfifteen' );

define( 'DISALLOW_FILE_EDIT', true );

define( 'AUTOSAVE_INTERVAL', 300 );
define( 'EMPTY_TRASH_DAYS', 7 );
define( 'WP_POST_REVISIONS', false );

$_lh_ = ( bool ) in_array( $_SERVER['REMOTE_ADDR'], array( '127.0.0.1', '::1' ) );

define( 'WP_DEBUG', $_lh_ );
define( 'WP_DEBUG_DISPLAY', $_lh_ );
define( 'WP_DEBUG_LOG', $_lh_ );

if ( $_lh_ ) {
	define( 'WP_HOME', 'http://localhost/client/project-2015-01/site' );
	define( 'WP_SITEURL', 'http://localhost/client/project-2015-01/site' );
	define( 'FS_METHOD', 'direct' );
}

unset( $_lh_ );
